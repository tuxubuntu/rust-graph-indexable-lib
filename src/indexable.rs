
use super::graph_lib::{
	GraphBasic,
	Graph,
	Initiated,
	Payloaded,
	errors:: { Result }
};

use super::{
	ERR_NODE_NOT_FOUND
};

pub type NodeID =usize;

#[derive(Debug)]
pub struct Indexable<T> {
	basic: GraphBasic<T>,
	index: Vec<Vec<usize>>,
	children: Vec<Vec<NodeID>>
}

impl<T> Indexable<T> {
	fn reg_id(&mut self, pid: &NodeID, pos: Vec<usize>) -> NodeID {
		let i = self.index.len();
		self.index.push(pos);
		let id = i;
		if self.children.get(pid.clone()).is_none() {
			let mut list = Vec::new();
			list.push(id.clone());
			self.children.push(list);
		} else {
			self.children.push(Vec::new());
		}
		self.children.get_mut(pid.clone()).unwrap().push(id.clone());
		id
	}
	fn get_pos(&self, id: &NodeID) -> Option<&Vec<usize>> {
		self.index.get(id.clone() as usize)
	}
	pub fn children_ids(&self, pid: &NodeID) -> Vec<NodeID> {
		if let Some(list) = self.children.get(pid.clone() as usize) {
			list.clone()
		} else {
			Vec::new()
		}
	}
	pub fn add_child(&mut self, pid: &NodeID, child: GraphBasic<T>) -> Result<NodeID> {
		match self.get(pid) {
			Some(_) => {
				let parent_len = self.get(pid).unwrap().len();
				let mut pos = self.get_pos(pid).cloned().unwrap();
				pos.push(parent_len);
				self.get_mut(pid).unwrap().add(child);
				Ok(self.reg_id(pid, pos))
			},
			None => Err(ERR_NODE_NOT_FOUND),
		}
	}
	pub fn make_child(&mut self, pid: &NodeID) -> NodeID {
		let graph = GraphBasic::new();
		self.add_child(pid, graph).unwrap()
	}
	pub fn make_child_from(&mut self, pid: &NodeID, payload: T) -> NodeID {
		let graph = GraphBasic::from_payload(payload);
		self.add_child(pid, graph).unwrap()
	}
	pub fn total_count(&self) -> usize {
		self.index.len()
	}
}

impl<T> Graph<GraphBasic<T>, NodeID> for Indexable<T> {
	fn add(&mut self, graph: GraphBasic<T>) -> NodeID {
		let pos = vec![self.basic.len()];
		let pid = 0;
		let id = self.reg_id(&pid, pos);
		self.basic.add(graph);
		id
	}
	fn len(&self) -> usize {
		self.basic.len()
	}
	fn children(&self) -> &Vec<GraphBasic<T>> {
		self.basic.children()
	}
	fn get(&self, id: &NodeID) -> Option<&GraphBasic<T>> {
		let pos = self.get_pos(id).unwrap();
		let mut graph = &self.basic;
		for i in pos {
			if let Some(child) = graph.get(i) {
				graph = child;
			} else {
				return None
			}
		}
		Some(graph)
	}
	fn get_mut(&mut self, id: &NodeID) -> Option<&mut GraphBasic<T>> {
		let pos = self.get_pos(id).cloned().unwrap();
		fn helper<'a, T>(pos: &Vec<usize>, cursor: usize, graph: &'a mut GraphBasic<T>) -> Option<&'a mut GraphBasic<T>> {
			let id = pos[cursor];
			if let Some(child) = graph.get_mut(&id) {
				let cursor = cursor+1;
				if pos.len() == cursor {
					Some(child)
				} else {
					helper(pos, cursor, child)
				}
			} else {
				None
			}
		}
		helper(&pos, 0, &mut self.basic)
	}
}

impl<T> Initiated<GraphBasic<T>, NodeID> for Indexable<T> {
	fn new() -> Indexable<T> {
		Indexable {
			basic: GraphBasic::new(),
			index: Vec::new(),
			children: Vec::new()
		}
	}
	fn make(&mut self) -> NodeID {
		let graph = GraphBasic::new();
		self.add(graph)
	}
}

impl<T> Payloaded<T, GraphBasic<T>, NodeID> for Indexable<T> {
	fn payload_get(&self) -> Option<&T> {
		self.basic.payload_get()
	}
	fn payload_mut(&mut self) -> Option<&mut T> {
		self.basic.payload_mut()
	}
	fn payload_set(&mut self, payload: T) {
		self.basic.payload_set(payload)
	}
	fn from_payload(payload: T) -> Indexable<T> {
		let mut graph = Indexable::new();
		graph.payload_set(payload);
		graph
	}
	fn make_from(&mut self, payload: T) -> NodeID {
		let mut graph = GraphBasic::new();
		graph.payload_set(payload);
		self.add(graph)
	}
}

extern crate graph_lib;

mod indexable;

pub use self::indexable::{
	Indexable,
	NodeID
};

#[cfg(test)]
mod tests;

use self::graph_lib::{
	errors:: { GraphError }
};

const ERR_NODE_NOT_FOUND: GraphError = GraphError("Node not found");


use super::graph_lib::{
	Graph,
	Initiated,
	Payloaded
};

#[test]
fn getters() {
	use *;
	let mut index = Indexable::new();
	let a = index.make_from(1);
	let b = index.make_child_from(&a, 2);
	let c = index.make_from(3);
	let d = index.make_child_from(&c, 4);
	assert_eq!(index.get(&a).unwrap().payload_get().unwrap(), &1);
	assert_eq!(index.get(&b).unwrap().payload_get().unwrap(), &2);
	assert_eq!(index.get(&c).unwrap().payload_get().unwrap(), &3);
	assert_eq!(index.get(&d).unwrap().payload_get().unwrap(), &4);
	assert_eq!(index.get_mut(&a).unwrap().payload_get().unwrap(), &1);
	assert_eq!(index.get_mut(&b).unwrap().payload_get().unwrap(), &2);
	assert_eq!(index.get_mut(&c).unwrap().payload_get().unwrap(), &3);
	assert_eq!(index.get_mut(&d).unwrap().payload_get().unwrap(), &4);
}

#[test]
fn identify() {
	use *;
	let mut index = Indexable::new();
	let id = index.make_from(true);
	index.get_mut(&id).unwrap().payload_set(false);
	assert_eq!(index.get_mut(&id).unwrap().payload_get().unwrap(), &false);
}
